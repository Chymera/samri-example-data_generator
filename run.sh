#!/usr/bin/env bash

python make_bids.py || exit 1
python preprocess.py || exit 1
python collapse.py || exit 1
python glm.py || exit 1
bash make_archive.sh || exit 1
rm -rf /var/tmp/samri_bidsdata || exit 1
