from samri.pipelines.preprocess import generic, legacy
from samri.pipelines import manipulations

bids_base = '/var/tmp/samri_bidsdata/bids'

# Preprocess all of the data:
generic(bids_base,
	"/usr/share/mouse-brain-atlases/dsurqec_200micron.nii",
	registration_mask="/usr/share/mouse-brain-atlases/dsurqec_200micron_mask.nii",
	functional_match={'acquisition':['EPIlowcov','EPI'],'subject':['4007']},
	structural_match={'acquisition':['TurboRARElowcov','TurboRARE'],'subject':['4007']},
	actual_size=True,
	out_base='/var/tmp/samri_bidsdata/preprocessing',
	workflow_name='generic',
	)
