from samri.utilities import collapse_by_path

bids_base = '/var/tmp/samri_bidsdata/bids'

collapse_by_path(
	'/var/tmp/samri_bidsdata/bids/sub-4007/ses-ofM/func/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-0_bold.nii.gz',
	'/var/tmp/samri_bidsdata/bids_collapsed/sub-4007/ses-ofM/func/sub-4007_ses-ofM_task-JogB_acq-EPIlowcov_run-0_bold.nii.gz'
	)
