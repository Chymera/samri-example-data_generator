# Usage

Execute the following master command to initialize the processing sequence:

```
./run.sh
```

# Requirements

* [SAMRI](https://github.com/IBT-FMI/SAMRI)
* Opto-fMRI data from the ETH/UZH Institute for Biomedical Engineering (yet unpublished)
