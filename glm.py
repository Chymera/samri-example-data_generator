from os import path
from samri.pipelines import glm

preprocess_base = '/var/tmp/samri_bidsdata/preprocessing/'

glm.l1(path.join(preprocess_base,'generic'),
	workflow_name='generic',
	habituation="confound",
	mask='/usr/share/mouse-brain-atlases/dsurqec_200micron_mask.nii',
	n_jobs_percentage=.33,
	match={'type':['cbv']},
	invert=True,
	out_base='/var/tmp/samri_bidsdata/l1'
	)
glm.l1(path.join(preprocess_base,'generic'),
	workflow_name='generic',
	habituation="confound",
	mask='/usr/share/mouse-brain-atlases/dsurqec_200micron_mask.nii',
	n_jobs_percentage=.33,
	match={'type':['bold']},
	out_base='/var/tmp/samri_bidsdata/l1'
	)
