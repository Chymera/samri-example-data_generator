from samri.pipelines.reposit import bru2bids

data_dir = '~/ni_data/ofM.dr'

bru2bids(data_dir,
	inflated_size=False,
	functional_match={
		'subject':['4007','5697'],
		'session':['ofM','ofMaF'],
		'acquisition':['EPIlowcov','EPI'],
		},
	structural_match={
		'subject':['4007','5697'],
		'session':['ofM','ofMaF'],
		'acquisition':['TurboRARElowcov','TurboRARE'],
		},
	out_base='/var/tmp/samri_bidsdata'
	)
